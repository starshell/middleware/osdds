# Unofficial bindings to OpenSplice DDS

[![Crates.io](https://img.shields.io/crates/v/osdds.svg)](https://crates.io/crates/osdds) [![Crates.io](https://img.shields.io/crates/d/osdds.svg)](https://crates.io/crates/osdds) [![license](https://img.shields.io/crates/l/qwerty.svg)](https://gitlab.com/starshell/middleware/osdds/blob/master/LICENSE) [![Coverage Status](https://codecov.io/gl/starshell/middleware/osdds/branch/master/graph/badge.svg)](https://codecov.io/gl/starshell/middleware/osdds) [![coverage report](https://gitlab.com/starshell/middleware/osdds/badges/master/coverage.svg)](https://gitlab.com/starshell/middleware/osdds/commits/master)

Linux: [![Build status](https://gitlab.com/starshell/middleware/osdds/badges/master/pipeline.svg)](https://gitlab.com/starshell/middleware/osdds/commits/master)
Windows: [![Build status](https://ci.appveyor.com/api/projects/status/k7ccce79080tfu18/branch/master?svg=true)](https://ci.appveyor.com/project/Eudoxier/osdds/branch/master)

&nbsp;

--------------------------------------------------------------------------------

**This is not an official product of, nor associated with, ADLINK (formerly PrismTech).**

> OpenSplice is a product of ADLINK, and thier trademark.

--------------------------------------------------------------------------------

&nbsp;

A Rust API for OpenSplice DDS (Data Distribution Service). DDS is a [standard by the Object Management Group (OMG) consortium](https://github.com/ADLINK-IST/opensplice/releases).

## Building

OpenSplice must be installed and the `release.com` script must be sourced to provide environment variables before building. You can find the source in [thier GitHub repository](https://github.com/ADLINK-IST/opensplice) and they have great documentation on [building from source](http://www.prismtech.com/dds-community/building). They also provide a large number of [releases for download](https://github.com/ADLINK-IST/opensplice/releases).

## Usage

Add `osdds` as a dependency in your `Cargo.toml` to use from crates.io:

```toml
[dependencies]
osdds = "0.1.0"
```

Then add `extern crate osdds;` to your crate root and run `cargo build` or `cargo update && cargo build` for your project. Detailed documentation for releases can be found on [docs.rs](https://docs.rs/osdds/). This crate is not meant to be used directly but to provide only the wrapper and for other libraries to use and implement a rusty API on top.

## Contributing

The project is mirrored to GitHub, but all development is done on GitLab. Please use the [GitLab issue tracker](https://gitlab.com/starshell/middleware/osdds). Don't have a GitLab account? Just email `incoming+starshell/middleware/osdds@gitlab.com` and those emails automatically become issues (with the comments becoming the email conversation).

To contribute to **osdds**, please see [CONTRIBUTING](CONTRIBUTING.md).

## License

**osdds** is distributed under the terms of the **MPL 2.0** license. If this does not suit your needs for some reason please feel free to contact me, or open an issue.

See [LICENSE](LICENSE).
