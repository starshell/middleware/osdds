/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

extern crate bindgen;

use std::env;
use std::path::PathBuf;

fn main() {
    check_clang_version();

    let build_env = BuildEnv::new();
    println!("OpenSplice environment set to:\n{:?}", build_env);

    // Tell cargo to tell rustc to link the OpenSplice shared library.
    for ld_library_path in build_env.ld_library_path {
        println!("cargo:rustc-link-search=native={}", ld_library_path);
    }
    println!("cargo:rustc-link-lib=dylib=dcpssac");

    // The bindgen::Builder is the main entry point
    // to bindgen, and lets you build up options for
    // the resulting bindings.
    //
    // FIXME: See issue #1, waiting on bindgen fix.
    let bindings = bindgen::Builder::default()
        .header("opensplice_wrapper.h")
        .clang_args(build_env.cpath.iter().map(|path| {
            let mut arg = "-I".to_string();
            arg.push_str(path);
            arg
        }))
        .clang_arg(build_env.sac_include)
        .blacklist_type("_bindgen_ty_24")
        .blacklist_type("max_align_t")
        .generate_comments(true)
        .generate()
        .expect("Unable to generate bindings");

    // Write the bindings to the $OUT_DIR/bindings.rs file.
    let out_path = PathBuf::from(env::var("OUT_DIR").unwrap());
    bindings
        .write_to_file(out_path.join("bindings.rs"))
        .expect("Couldn't write bindings!");
}

fn check_clang_version() {
    let version = bindgen::clang_version();
    println!("Clang version: {:?}", &version);

    if let Some((major, minor)) = version.parsed {
        if major < 3 || (major == 3 && minor < 9) {
            println!("Warning: bindgen recommends clang >= 3.9");
        }
    }
}

#[derive(Debug)]
struct BuildEnv {
    ospl_home: String,
    sac_include: String,
    ld_library_path: Vec<String>,
    cpath: Vec<String>,
}

impl BuildEnv {
    // Construct a BuildEnv from environment variables.
    //
    // This method expects all environment variables to be present that
    // would be set by sourcing an OpenSplice installtions `release.com`
    // script.
    //
    // # Panics
    //
    // This method panics if any needed environment variables are not set.
    fn new() -> BuildEnv {
        let source_msg =
            "Have you sourced your OpenSplice installation's `release.com`?".to_string();
        let ospl_home;
        let sac_include;
        let ld_library_path: Vec<String>;
        let cpath: Vec<String>;

        if let Ok(ospl_home_env) = env::var("OSPL_HOME") {
            ospl_home = ospl_home_env.clone();
            sac_include = [ospl_home_env, "/include/dcps/C/SAC".to_string()].join("");
        } else {
            panic!(
                "OSPL_HOME environment variable must be set.\n{}\n",
                source_msg
            );
        };

        if let Ok(ld_library_path_env) = env::var("LD_LIBRARY_PATH") {
            ld_library_path = env::split_paths(&ld_library_path_env)
                .into_iter()
                .map(|path| path.to_str().expect("Unable to parse path").to_string())
                .collect();
        } else {
            panic!(
                "LD_LIBRARY_PATH environment variable must be set.\n{}\n",
                source_msg
            );
        };

        if let Ok(cpath_env) = env::var("CPATH") {
            cpath = env::split_paths(&cpath_env)
                .into_iter()
                .map(|path| path.to_str().expect("Unable to parse path").to_string())
                .collect();
        } else {
            panic!("CPATH environment variable must be set.\n{}\n", source_msg);
        };

        BuildEnv {
            ospl_home,
            sac_include,
            ld_library_path,
            cpath,
        }
    }
}
