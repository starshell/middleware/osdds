# Unofficial bindings to OpenSplice DDS

[![Crates.io](https://img.shields.io/crates/v/opensplice-sys.svg)](https://crates.io/crates/opensplice-sys) [![Crates.io](https://img.shields.io/crates/d/opensplice-sys.svg)](https://crates.io/crates/opensplice-sys) [![license](https://img.shields.io/crates/l/qwerty.svg)](https://gitlab.com/starshell/middleware/opensplice-sys/blob/master/LICENSE) [![Coverage Status](https://codecov.io/gl/starshell/middleware/opensplice-sys/branch/master/graph/badge.svg)](https://codecov.io/gl/starshell/middleware/opensplice-sys)

Linux: [![Build status](https://gitlab.com/starshell/middleware/opensplice-sys/badges/master/pipeline.svg)](https://gitlab.com/starshell/middleware/opensplice-sys/commits/master)
Windows: [![Build status](https://ci.appveyor.com/api/projects/status/k7ccce79080tfu18/branch/master?svg=true)](https://ci.appveyor.com/project/Eudoxier/opensplice-sys/branch/master)

&nbsp;

--------------------------------------------------------------------------------

**This is not an official product of, nor associated with, ADLINK (formerly PrismTech).**

> OpenSplice is a product of ADLINK, and thier trademark. It is used here, with the customarily appended **-sys**, only to specify the origin of the bindings and avoid any confusion that this is an original DDS implementation.

--------------------------------------------------------------------------------

&nbsp;

Rust bindings for OpenSplice DDS (Data Distribution Service) protocol C99 API. DDS is a [standard by the Object Management Group (OMG) consortium](https://github.com/ADLINK-IST/opensplice/releases).

## Building

OpenSplice must be installed and the `release.com` script must be sourced to provide environment variables before building. You can find the source in [thier GitHub repository](https://github.com/ADLINK-IST/opensplice) and they have great documentation on [building from source](http://www.prismtech.com/dds-community/building). They also provide a large number of [releases for download](https://github.com/ADLINK-IST/opensplice/releases).

## Usage

Add `opensplice-sys` as a dependency in your `Cargo.toml` to use from crates.io:

```toml
[dependencies]
opensplice-sys = "0.1.0"
```

Then add `extern crate opensplice-sys;` to your crate root and run `cargo build` or `cargo update && cargo build` for your project. Detailed documentation for releases can be found on [docs.rs](https://docs.rs/opensplice-sys/). This crate is not meant to be used directly but to provide only the wrapper and for other libraries to use and implement a rusty API on top.

## Contributing

The project is mirrored to GitHub, but all development is done on GitLab. Please use the [GitLab issue tracker](https://gitlab.com/starshell/middleware/opensplice-sys). Don't have a GitLab account? Just email `incoming+starshell/middleware/opensplice-sys@gitlab.com` and those emails automatically become issues (with the comments becoming the email conversation).

To contribute to **opensplice-sys**, please see [CONTRIBUTING](CONTRIBUTING.md).

## License

**opensplice-sys** is distributed under the terms of the **MPL 2.0** license. If this does not suit your needs for some reason please feel free to contact me, or open an issue.

See [LICENSE](LICENSE).
