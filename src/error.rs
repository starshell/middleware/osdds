//! DDS error handling.

use opensplice_sys;
use std::error::Error;
use std::fmt;

/// DDS Result
///
/// DDS uses return codes instead of results, this helps to recieve a
/// result and handel it in a Rusty way.
#[derive(Debug)]
enum DdsResult {
    /// Successful return.
    DdsOk = opensplice_sys::DDS_RETCODE_OK as isize,
    /// Generic, unspecified error.
    DdsError = opensplice_sys::DDS_RETCODE_ERROR as isize,
    /// Unsupported operation.
    ///
    /// Can only be returned by operations that are optional.
    Unsupported = opensplice_sys::DDS_RETCODE_UNSUPPORTED as isize,
    /// Illegal parameter value.
    BadParameter = opensplice_sys::DDS_RETCODE_BAD_PARAMETER as isize,
    /// A precondition for the operation was not met.
    PreconditionNotMet = opensplice_sys::DDS_RETCODE_PRECONDITION_NOT_MET as isize,
    /// Service ran out of the resources needed to complete the operation.
    OutOfResources = opensplice_sys::DDS_RETCODE_OUT_OF_RESOURCES as isize,
    /// Operation invoked on an Entity that is not yet enabled.
    NotEnabled = opensplice_sys::DDS_RETCODE_NOT_ENABLED as isize,
    /// Application attempted to modify an immutable QosPolicy.
    ImmutablePolicy = opensplice_sys::DDS_RETCODE_IMMUTABLE_POLICY as isize,
    /// Application policies invalid.
    ///
    /// Application specified a set of policies that are not consistent with
    /// each other.
    InconsistentPolicy = opensplice_sys::DDS_RETCODE_INCONSISTENT_POLICY as isize,
    /// The object target of this operation has already been deleted.
    AlreadyDeleted = opensplice_sys::DDS_RETCODE_ALREADY_DELETED as isize,
    /// The operation timed out.
    Timeout = opensplice_sys::DDS_RETCODE_TIMEOUT as isize,
    /// Operation successful, but no data was produced.
    ///
    /// Indicates a transient situation where the operation did not return any
    /// data but there is no inherent error.
    NoData = opensplice_sys::DDS_RETCODE_NO_DATA as isize,
    /// Opperation not permitted error.
    ///
    /// An operation was invoked on an inappropriate object or at an
    /// inappropriate time (as determined by policies set by the specification
    /// or the Service implementation). There is no precondition that could be
    /// changed to make the operation succeed.
    IllegalOperation = opensplice_sys::DDS_RETCODE_ILLEGAL_OPERATION as isize,
}

/*
pub fn check_status(return_code: DDS_ReturnCode_t) -> DdsResult {
    match return_code {
    }
}
*/

#[derive(Debug)]
pub struct DdsError;

impl fmt::Display for DdsError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "DDS Error")
    }
}

impl Error for DdsError {
    fn description(&self) -> &str {
        "DDS Error"
    }

    fn cause(&self) -> Option<&Error> {
        None
    }
}
