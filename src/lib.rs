#[macro_use]
extern crate log;
extern crate opensplice_sys;

pub mod error;
pub mod participant;
pub mod qos;

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
