//! Participants

use error::DdsError;
use opensplice_sys;
use opensplice_sys::DDS_DomainParticipantFactory_create_participant;
use opensplice_sys::DDS_DomainParticipantFactory_delete_participant;
use opensplice_sys::DDS_DomainParticipantFactory_get_instance;
use qos::DdsQosDefaults;
use std;
use std::error::Error;

pub struct DomainParticipantBuilder {
    domain_id: Option<opensplice_sys::DDS_DomainId_t>,
    qos: Option<*const opensplice_sys::DDS_DomainParticipantQos>,
    listener: Option<*const opensplice_sys::DDS_DomainParticipantListener>,
    mask: Option<opensplice_sys::DDS_StatusMask>,
}

impl DomainParticipantBuilder {
    pub fn new() -> Self {
        DomainParticipantBuilder {
            domain_id: None,
            qos: None,
            listener: None,
            mask: None,
        }
    }

    /// Specify a domain ID.
    ///
    /// Must match the domain Id of one of the domains currently running on this node.
    pub fn domain_id(&mut self, domain_id: opensplice_sys::DDS_DomainId_t) -> &mut Self {
        self.domain_id = Some(domain_id);
        self
    }

    /// Specify QoS to use.
    pub fn qos(&mut self, qos: *const opensplice_sys::DDS_DomainParticipantQos) -> &mut Self {
        self.qos = Some(qos);
        self
    }

    /// Specify a domain participant listener.
    ///
    /// By default all events are treated as a no-op.
    pub fn listener(
        &mut self,
        listener: *const opensplice_sys::DDS_DomainParticipantListener,
    ) -> &mut Self {
        self.listener = Some(listener);
        self
    }

    /// Specify a bit mask identifying the status events on which it should trigger.
    ///
    /// By default status changes are ignored. A DDS domain participant has
    /// no factory to which it can propigate events, so technically it does
    /// not matter what bit mask is set.
    pub fn mask(&mut self, mask: opensplice_sys::DDS_StatusMask) -> &mut Self {
        self.mask = Some(mask);
        self
    }

    /// Consumes `self` and returns a `DomainParticipant`.
    pub fn create_participant(self) -> Result<DomainParticipant, DdsError> {
        let domain_participant: opensplice_sys::DDS_DomainParticipant;
        let dpf = get_domain_participant_factory()?;
        unsafe {
            domain_participant = DDS_DomainParticipantFactory_create_participant(
                dpf,
                self.domain_id
                    .unwrap_or(opensplice_sys::DDS_DOMAIN_ID_DEFAULT as i32),
                self.qos.unwrap_or(DdsQosDefaults::participant()),
                self.listener.unwrap_or(std::ptr::null()),
                self.mask.unwrap_or(opensplice_sys::DDS_STATUS_MASK_NONE),
            );
            if domain_participant.is_null() {
                return Err(DdsError);
            }
        }
        let destroyed = false;
        Ok(DomainParticipant {
            domain_participant,
            destroyed,
        })
    }
}

pub struct DomainParticipant {
    domain_participant: opensplice_sys::DDS_DomainParticipant,
    destroyed: bool,
}

impl DomainParticipant {
    /// Optionally allows for explicit deletion.
    ///
    /// Although failure of participant deletion is an exceptional
    /// circumstance, the possibility may need to be caught in critical
    /// code. This will allow manual handling of that.
    // Consider `#[must_use]` here.
    pub fn destroy(&mut self) -> Result<(), DdsError> {
        if self.destroyed {
            return Ok(());
        }
        let dpf = get_domain_participant_factory()?;
        unsafe {
            let status =
                DDS_DomainParticipantFactory_delete_participant(dpf, self.domain_participant);
            if status == opensplice_sys::DDS_RETCODE_OK as i32 {
                self.destroyed = true;
                return Ok(());
            }
        }
        Err(DdsError)
    }
}

impl Drop for DomainParticipant {
    fn drop(&mut self) {
        if !self.destroyed {
            if let Err(err) = self.destroy() {
                if let Some(cause) = err.cause() {
                    error!("{}: {}", err.description(), cause);
                } else {
                    error!("{}", err.description());
                }
            }
        }
    }
}

fn get_domain_participant_factory() -> Result<opensplice_sys::DDS_DomainParticipantFactory, DdsError>
{
    let dpf: opensplice_sys::DDS_DomainParticipantFactory;
    unsafe {
        dpf = DDS_DomainParticipantFactory_get_instance();
        if dpf.is_null() {
            return Err(DdsError);
        }
    }
    Ok(dpf)
}
