//! QoS

use opensplice_sys;

pub struct DdsQosDefaults {}

impl DdsQosDefaults {
    pub fn participant() -> *const opensplice_sys::DDS_DomainParticipantQos {
        unsafe { return opensplice_sys::DDS_PARTICIPANT_QOS_DEFAULT }
    }

    pub fn publisher() -> *const opensplice_sys::DDS_PublisherQos {
        unsafe { return opensplice_sys::DDS_PUBLISHER_QOS_DEFAULT };
    }

    pub fn subscriber() -> *const opensplice_sys::DDS_SubscriberQos {
        unsafe { return opensplice_sys::DDS_SUBSCRIBER_QOS_DEFAULT };
    }
}
