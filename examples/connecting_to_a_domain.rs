//! Connecting to a domain.

extern crate osdds;

fn main() {
    let domain_participant = osdds::participant::DomainParticipantBuilder::new()
        .create_participant()
        .expect("Creating Participant failed");
    println!("Success!!!!")
}
