# Chat example IDL files

[![Crates.io](https://img.shields.io/crates/v/chat-idl.svg)](https://crates.io/crates/chat-idl) [![Crates.io](https://img.shields.io/crates/d/chat-idl.svg)](https://crates.io/crates/chat-idl) [![license](https://img.shields.io/crates/l/qwerty.svg)](https://gitlab.com/starshell/middleware/chat-idl/blob/master/LICENSE) [![Coverage Status](https://codecov.io/gl/starshell/middleware/chat-idl/branch/master/graph/badge.svg)](https://codecov.io/gl/starshell/middleware/chat-idl)

Linux: [![Build status](https://gitlab.com/starshell/middleware/chat-idl/badges/master/pipeline.svg)](https://gitlab.com/starshell/middleware/chat-idl/commits/master)
Windows: [![Build status](https://ci.appveyor.com/api/projects/status/k7ccce79080tfu18/branch/master?svg=true)](https://ci.appveyor.com/project/Eudoxier/chat-idl/branch/master)

&nbsp;

IDL files and wrapper for the OpenSplice chat tutorial.

## Building

OpenSplice must be installed and the `release.com` script must be sourced to provide environment variables before building. You can find the source in [thier GitHub repository](https://github.com/ADLINK-IST/opensplice) and they have great documentation on [building from source](http://www.prismtech.com/dds-community/building). They also provide a large number of [releases for download](https://github.com/ADLINK-IST/opensplice/releases).

## Usage

Add `chat-idl` as a dependency in your `Cargo.toml` to use from crates.io:

```toml
[dependencies]
chat-idl = "0.1.0"
```

Then add `extern crate chat-idl;` to your crate root and run `cargo build` or `cargo update && cargo build` for your project. Detailed documentation for releases can be found on [docs.rs](https://docs.rs/chat-idl/). This crate is not meant to be used directly but to provide only the wrapper and for other libraries to use and implement a rusty API on top.

## Contributing

The project is mirrored to GitHub, but all development is done on GitLab. Please use the [GitLab issue tracker](https://gitlab.com/starshell/middleware/chat-idl). Don't have a GitLab account? Just email `incoming+starshell/middleware/chat-idl@gitlab.com` and those emails automatically become issues (with the comments becoming the email conversation).

To contribute to **chat-idl**, please see [CONTRIBUTING](CONTRIBUTING.md).

## License

**chat-idl** is distributed under the terms of the **MPL 2.0** license. If this does not suit your needs for some reason please feel free to contact me, or open an issue.

See [LICENSE](LICENSE).
