# Contributing guide
Want to contribute to **chat-idl**? Awesome!
There are many ways you can contribute, see below.

## Opening issues
Open an issue to report bugs or to propose new features.

## Proposing pull requests
Merge requests are very welcome. Note that if you are going to propose drastic changes, be sure to open an issue for discussion first, to make sure that your merge request will be accepted before you spend effort coding it.

## Resources

A good reference is the [Cargo Book](https://doc.rust-lang.org/cargo/index.html) chapter on [build scripts](https://doc.rust-lang.org/cargo/reference/build-scripts.html).

### Bindgen

The tool used for generating bindings is [bindgen](https://github.com/rust-lang-nursery/rust-bindgen) and they have great documentation:

- [Users Guide](https://rust-lang-nursery.github.io/rust-bindgen)
- [API Reference](https://docs.rs/bindgen)
- [Tutorial Code (bzip2)](https://github.com/fitzgen/bindgen-tutorial-bzip2-sys)

### ADLINK OpenSplice

The Object Management Group (OMG) also provides [detailed specifications for the Data Distribution System (DDS)](https://www.omg.org/spec/category/data-distribution-service/). Adlink (formerly PrismTech) provides lot of [great documentation](http://www.prismtech.com/dds-community/resources/documentation) about OpenSplice. The most relevant documents are:

- [The DDS Tutorial](http://download.prismtech.com/docs/Vortex/apis/ospl/pdf/OpenSplice_DDSTutorial.pdf)
- [C Tutorial Guide](http://download.prismtech.com/docs/Vortex/pdfs/OpenSplice_Tutorial_C.pdf)
- [C99 OpenSplice Data Distribution Service Data-Centric Publish-Subscribe API](http://download.prismtech.com/docs/Vortex/apis/ospl/c99/html/index.html)
